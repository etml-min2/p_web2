<?php
require 'variables.php';
require ROOT . '/app/Config.php';
require 'functions.php';
require ROOT . '/app/Database.php';
require ROOT . '/app/Router.php';
require ROOT . '/app/Session.php';
require ROOT . '/app/Auth.php';
require ROOT . '/app/App.php';

spl_autoload_register(function ($class) {
    $class = str_replace('App\\', '', $class);
    require ROOT . '/app/' . $class . '.php';
});