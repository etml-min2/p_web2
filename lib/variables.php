<?php
session_start();
define('ROOT', dirname(__DIR__));
define('APP', ROOT . '/app');
define('BASE_URL', trim('\\', dirname($_SERVER['SCRIPT_NAME'])));
define('WEBROOT', BASE_URL . '/template');