<?php

function debug($var){
    echo "<pre>";
    print_r($var);
    echo "</pre>";
}

function get_header($theme = null){
    if(is_null($theme))
        $theme = \App\Config::getConfig()['theme'];

    include_once ROOT.'/template/'.$theme.'/header.php';
}

function get_footer($theme = null){
    if(is_null($theme))
        $theme = \App\Config::getConfig()['theme'];

    include_once ROOT.'/template/'.$theme.'/footer.php';
}

function get_flash(){
    if(session_status() != PHP_SESSION_ACTIVE)
        session_start();
    if (isset($_SESSION['flash'])): ?>
        <div class="flashBG container alert alert-dismissible alert-<?=$_SESSION['flash']['type']?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?=$_SESSION['flash']['message']?>
        </div>
    <?php
    unset($_SESSION['flash']);
    endif;

}

function set_flash($message, $type = "success"){
    if(session_status() != PHP_SESSION_ACTIVE)
        session_start();
    $_SESSION['flash']['message'] = $message;
    $_SESSION['flash']['type'] = $type;
}