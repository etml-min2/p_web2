<div class="container" >
    <div class="page-header">
        <h1 style="margin-top: 100px;">Modification de <?= $datas['cif']['cifTitle']; ?></h1>
    </div>
    <?php get_flash(); ?>
    <form action="<?= BASE_URL.'/cif/edit/'.$datas['cif']['idCif']; ?>" method="post" role="form">
        <div class="form-group">
            <label for="title">Titre</label>
            <input type="text" class="form-control" name="title" id="title" placeholder="Votre titre ici" value="<?= $datas['cif']['cifTitle']; ?>" autofocus required>
        </div>
        <div class="form-group">
            <label for="category">Catégorie</label>
            <select class="form-control" name="category" id="category">
                <?php foreach($datas['categories'] as $category): ?>
                    <option value="<?= $category['idCategory']; ?>" <?= $datas['cif']['fkCategory']==$category['idCategory'] ? 'selected' : ''; ?>><?= $category['catName']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" name="description" id="description" placeholder="Votre description ici"><?= $datas['cif']['cifDescription']; ?></textarea>
        </div>
        <button class="btn btn-primary" role="button" type="submit">Modifier</button>
    </form>
</div>

<script src="<?= BASE_URL.'/template/Web2/js/tinymce/tinymce.min.js'; ?>"></script>
<script>
    tinymce.init({
        selector: 'textarea',
        language: 'fr_CH',
        height: 300
    });
</script>