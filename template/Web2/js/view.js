/*!
 * Start Bootstrap - Grayscale Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery to collapse the navbar on scroll
function collapseNavbar() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
}

$(window).scroll(collapseNavbar);
$(document).ready(collapseNavbar);

// Flash fade out
$(document).ready(function() {
    setTimeout(function(){
        $(".flashBG").fadeOut("slow");
    },2500)
});

// Dialog
var modal = document.getElementById('modal');
var showBtn = document.getElementById('show');
var closeBtn = document.getElementById('close');

// Setup an event listener for the show button.
showBtn.addEventListener('click', function(e) {
    e.preventDefault();

    // Show the modal.
    dialogPolyfill.registerDialog(modal);
    modal.showModal();
});

// Setup an event listener for the close button.
closeBtn.addEventListener('click', function(e) {
    e.preventDefault();

    // Close the modal.
    modal.close();
});