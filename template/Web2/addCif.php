<div class="container" style="font-family: montserrat; margin-bottom: 10px;">
    <div class="page-header">
        <h1 style="margin-top: 100px;">Ajout d'une CIF</h1>
    </div>
    <?php get_flash(); ?>

    <form action="<?= BASE_URL.'/cif/add'; ?>" method="post" role="form">
        <div class="form-group">
            <label for="title">Titre</label>
            <input type="text" class="form-control" name="title" id="title" placeholder="Votre titre ici" autofocus required>
        </div>
        <div class="form-group">
            <label for="category">Catégorie</label>
            <select class="form-control" name="category" id="category">
                <?php foreach($datas as $category): ?>
                    <option value="<?= $category['idCategory']; ?>"><?= $category['catName']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" name="description" id="description" placeholder="Votre description ici"></textarea>
        </div>

        <button class="btn btn-primary" role="button" type="submit">Ajouter la CIF</button>
    </form>
</div>

<script src="<?= BASE_URL.'/template/Web2/js/tinymce/tinymce.min.js'; ?>"></script>
<script>
    tinymce.init({
        selector: 'textarea',
        language: 'fr_CH',
        height: 300
    });
</script>