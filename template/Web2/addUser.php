<?php
$add = false;
if(is_null($datas)){
    $add = true;
}
?>
<div class="container" style="font-family: montserrat; margin-bottom: 10px;">
    <div class="page-header">
        <h1 style="margin-top: 100px;"><?=$add ? "Ajout d'un utilisateur" : "Modification de vos informations"?></h1>
    </div>
    <?php get_flash(); ?>
    <form action="<?=BASE_URL?>/user/<?=$add ? 'add' : 'edit'?>" method="post" role="form">
        <div class="form-group col-md-12">
            <label for="add_username">Nom d'utilisateur</label>
            <input type="text" class="form-control" name="add_username" id="add_username" <?=$add ? "" : 'value="'.$datas['usePseudo'].'"' ?> placeholder="Votre nom d'utilisateur ici" autofocus required>
        </div>
        <div class="form-group col-md-6">
            <label for="add_password">Mot de passe</label>
            <input type="password" class="form-control" name="add_password" id="add_password" placeholder="Votre mot de passe ici" required>
        </div>
        <div class="form-group col-md-6">
            <label for="add_passwordConfirm">Confirmer votre mot de passe</label>
            <input type="password" class="form-control" name="add_passwordConfirm" id="add_passwordConfirm" placeholder="Votre mot de passe ici" required>
        </div>

        <div class="form-group col-md-12">
            <label for="add_firstName">Prénom</label>
            <input type="text" class="form-control" name="add_firstName" id="add_firstName" <?=$add ? "" : 'value="'.$datas['useFirstName'].'"' ?> placeholder="Votre prénom ici" required>
        </div>
        <div class="form-group col-md-12">
            <label for="add_lastName">Nom</label>
            <input type="text" class="form-control" name="add_lastName" id="add_lastName" <?=$add ? "" : 'value="'.$datas['useLastName'].'"' ?> placeholder="Votre nom ici" required>
        </div>

        <div class="col-md-12">
            <a href="<?=BASE_URL?>/user" style="margin-top: 10px;" class="btn btn-default pull-left">Annuler</a>
            <button style="margin-top: 10px;" class="btn btn-primary pull-right" role="button" type="submit"><?=$add ? "Créer l'utilisateur" : "Appliquer" ?></button>
        </div>
    </form>
</div>