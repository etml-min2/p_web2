<!-- 
ETML
Author : beguinju
Date : 13.04.2016
Description : 
-->
<div class="element container" style="width: 400px; font-family: montserrat;">
    <h4 style="margin-bottom: 20px;">Veuillez vous connecter</h4>
    <form class="form" id="formLogin" action="<?= BASE_URL; ?>/login<?= isset($_GET['redirect']) ? '?redirect='.$_GET['redirect'] : ''; ?>" method="post">
        <input class="form-control" name="username" id="username" type="text"
               placeholder="Nom d'utilisateur" style="margin-bottom: 15px">
        <input class="form-control" name="password" id="password" type="password"
               placeholder="Mot de passe" style="margin-bottom: 20px">
        <button type="submit" id="btnLogin" class="btn btn-primary">Connexion</button>
        <button type="button" onclick="location.href='<?=BASE_URL?>/user/add'" id="btnRegister" class="btn btn-default btn-sm pull-right">
            Créer un compte
        </button>
    </form>
</div>
<?php get_flash(); ?>