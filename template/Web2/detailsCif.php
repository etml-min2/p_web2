<?php
use App\AppFactory;
$auth = AppFactory::getAuth();
$session = AppFactory::getSession();
?>

<div style="background: rgba(255, 255, 255, 0.2)">
    <div class="container" style="padding-top: 100px;">
        <?php get_flash(); ?>
        <div class="page-header">
            <a href="<?= BASE_URL . '/cif'; ?>" class="btn btn-default btn-sm" style="vertical-align: top; margin-right: 5px; margin-top: 4px;"><i class="fa fa-arrow-left"></i> CIF</a>
            <h1 style="display: inline-block">Détails de <?= $datas['cif']['cifTitle']; ?>
                <?php if($auth->isLogged() && $session->read('auth')['idUser'] == $datas['cif']['fkUser']): ?>
                    <a href="<?= BASE_URL.'/cif/edit/'.$datas['cif']['idCif']; ?>" class="btn btn-warning"><i class="fa fa-edit"></i> Modifier</a>
                    <a href="<?= BASE_URL.'/cif/delete/'.$datas['cif']['idCif']; ?>" class="btn btn-danger" onclick="return confirm('Voulez-vous vraiment supprimer cette CIF ?');"><i class="fa fa-trash"></i> Supprimer</a>
                <?php endif; ?>
            </h1>
        </div>
        <div class="center-details">
            <table class="table">
                <tr>
                    <td class="details-title">Titre</th>
                    <td class="details-content"><p style="font-size: 24px"><?= $datas['cif']['cifTitle']; ?></p></td>
                </tr>
                <tr>
                    <td class="details-title">Créateur</th>
                    <td class="details-content">
                        <a href="<?=BASE_URL.'/user/'.$datas['cif']['fkUser'] ?>" style="text-decoration: none; color: inherit;">
                            <p style="font-size: 24px">
                                <?=$datas['cif']['usePseudo']?>
                            </p>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="details-title">Catégorie</th>
                    <td class="details-content">
                        <a href="<?=BASE_URL.'/cif?cat='.$datas['cif']['fkCategory'] ?>" style="text-decoration: none; color: inherit;">
                            <p style="font-size: 24px">
                                <?= $datas['cif']['catName']; ?>
                            </p>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="details-title">Description</th>
                    <td class="details-content"><?= $datas['cif']['cifDescription']; ?></td>
                </tr>
                <tr>
                    <td class="details-title">Nombre d'évaluations</th>
                    <td class="details-content"><p style="font-size: 24px"><?=$datas['eval']['noteCount'] ?></p></td>
                </tr>
                <tr>
                    <td class="details-title">Évaluation moyenne</th>
                    <td class="details-content">
                        <?php if ($datas['eval']['noteCount'] > 0): ?>
                            <input type="hidden" data-readonly value="<?=$datas['eval']['noteAverage']?>" class="rating" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x">
                        <?php else: ?>
                            Cette cif n'as pas encore été noté.
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <dialog id="modal">
        <form class="form" id="formComment" action="<?=BASE_URL?>/cif/addComment?id=<?=$datas['cif']['idCif'] ?>" method="post">
            <h4>Ajouter un commentaire</h4>
            <div class="form-group">
                <label for="note" class="control-label">Note</label><br>
                <input type="hidden" name="note" class="rating" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x">
            </div>
            <div class="form-group">
                <label class="control-label" for="title">Titre</label>
                <input class="form-control input-sm" type="text" id="title" name="title" autofocus required>
            </div>
            <div class="form-group">
                <label for="comment" class="control-label">Commentaire</label>
                <div>
                    <textarea class="form-control" rows="3" id="comment" name="comment"></textarea>
                    <span class="help-block">Ajoutez un commentaire en relation à cette cif.</span>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Envoyer</button>
            <button id="close" class="btn btn-default">Fermer</button>
        </form>
    </dialog>

    <div class="container center-details" style="margin-top: 20px;">
        <h2>
            Notes & Commentaires
            <?php if(AppFactory::getAuth()->isLogged()): ?>
                <a id="show" class="btn btn-primary"><i class="fa fa-pencil"></i> Commenter</a>
            <?php else: ?>
                <a href="<?=BASE_URL?>/login?redirect=<?=$_GET['url']?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Commenter</a>
            <?php endif; ?>
        </h2>
    <?php
    if (!empty($datas['comment'])) {
        foreach ($datas['comment'] as $comment): ?>

            <div class="panel panel-primary" style="border: none;">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <?= $comment['evaTitle'] ?>
                        <?php for ($i = 0; $i < 5; $i++): ?>
                             <i class="fa fa-star<?=$i < $comment['evaNote'] ? "" : "-o" ?>"></i>
                        <?php endfor; ?>
                        <a href="<?=BASE_URL.'/user/'.$comment['idUser'] ?>"><span class="light pull-right"><?= $comment['usePseudo'] ?></span></a>
                    </h3>

                </div>
                <?php if (!empty($comment['evaDescription'])): ?>
                <div class="panel-body">
                    <?= $comment['evaDescription'] ?>
                </div>
                <?php endif; ?>
            </div>

        <?php endforeach;
    }else{
        echo "<h5>Soyez le premier à commenter cette cif.</h5>";
    }
    ?>
    </div>
</div>