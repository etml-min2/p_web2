<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/png" href="<?=WEBROOT?>/Web2/img/ic_done_black_16dp_1x.png">

    <title>Projet Web 2</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= WEBROOT; ?>/Web2/css/flatly.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= WEBROOT; ?>/Web2/css/home.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= WEBROOT; ?>/Web2/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= WEBROOT; ?>/Web2/css/google-fonts-lora.css" rel="stylesheet">
    <link href="<?= WEBROOT; ?>/Web2/css/google-fonts-montserrat.css" rel="stylesheet">
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

<!-- Navigation -->
<nav class="navbar navbar-custom navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <i class="fa fa-play-circle"></i> <span class="light">Projet</span> Web 2
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
            <ul class="nav navbar-nav">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li>
                    <a href="<?= BASE_URL; ?>/"><i class="fa fa-home"></i> Accueil</a>
                </li>
                <li>
                    <a href="<?= BASE_URL; ?>/cif"><i class="fa fa-check"></i> CIF</a>
                </li>
                <li>
                    <a href="<?= BASE_URL; ?>/cif/add"><i class="fa fa-plus"></i> Nouveau</a>
                </li>
                <?php if (isset($_SESSION['auth'])) { ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown"
                           id="navLogin"><i class="fa fa-user"></i> <?= $_SESSION['auth']['usePseudo'] ?></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?=BASE_URL?>/user"><i class="fa fa-user"></i> Espace membre</a></li>
                            <li><a href="<?=BASE_URL?>/logout" style="color: darkred;"><i class="fa fa-sign-out"></i> Déconnexion</a></li>
                        </ul>
                    </li>
                <?php } else { ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin"><i class="fa fa-user"></i> Connexion</a>

                        <div class="dropdown-menu" style="padding:17px; width: 300px;">
                            <form class="form" id="formLogin" action="<?=BASE_URL?>/login" method="post">
                                <input class="form-control" name="username" id="username" type="text"
                                       placeholder="Nom d'utilisateur" style="margin-bottom: 15px">
                                <input class="form-control" name="password" id="password" type="password"
                                       placeholder="Mot de passe" style="margin-bottom: 15px">
                                <button type="submit" id="btnLogin" class="btn btn-primary btn-sm">Connexion</button>
                                <button type="button" onclick="location.href='<?=BASE_URL?>/user/add'" id="btnRegister" class="btn btn-default btn-sm pull-right">
                                    Créer un compte
                                </button>
                            </form>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Intro Header -->
<header class="intro">
    <div class="intro-body">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h1 class="brand-heading"><span class="light">Projet</span> Web2</h1>
                    <a href="#about" class="btn btn-circle page-scroll">
                        <i class="fa fa-angle-double-down animated"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- About Section -->
<section id="about" class="container content-section text-center">

    <h2>Les 5 dernières CIFs</h2>
    <div style="text-align: center;">
        <?php foreach($datas['cif'] as $cif): ?>
            <div class="col-md-3" style="display: inline-block !important; float: none; vertical-align: top;">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="<?= BASE_URL ?>/user/<?= $cif['idUser'] ?>"><?= $cif["usePseudo"] ?></a>
                        </h3>
                    </div>
                    <a href="<?= BASE_URL . '/cif/' . $cif['idCif'] ?>" class="list-group-item">
                        <div class="cif-title"><?= $cif['cifTitle'] ?></div>
                        <div class="cif-badge">
                            <span class="badge"><?= $cif['catName']; ?></span>
                        </div>
                    </a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h2>À propos du projet</h2>
            <p>Ce projet vise à consolider les connaissances de mise en œuvre des modules ICT 133 et 151.</p>
            <p>Ce site web permet de visualiser la listes des CIF (Choses Importantes à Faire) stockées en base de données, ainsi que leur évaluation.</p>
        </div>
    </div>
</section>

<!-- Link Section -->
<section id="download" class="content-section text-center">
    <div class="download-section">
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Visualiser les CIF</h2>
                <p>Vous pouvez visualisez toutes les CIF de l'application via le bouton ci-dessous</p>
                <a href="<?= BASE_URL.'/cif'; ?>" class="button button-default btn-lg">Acceder au CIF</a>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section id="contact" class="container content-section text-center">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h2>Contact</h2>

            <p>N'hésitez pas à nous contactez pour toutes suggestions, questions ou feedback à <a href="mailto:beguinju@etml.educanet2.ch">beguinju@etml.educanet2.ch</a> ou
                <a href="mailto:balsigergi@etml.educanet2.ch">balsigergi@etml.educanet2.ch</a></p>
        </div>
    </div>
</section>

<!-- Space -->
<section class="content-section">
    <div class="download-section" style="height: 350px;"></div>
</section>
<!-- Footer -->
<footer>
    <div class="container text-center">
        <p>Copyright &copy; Balsiger & Béguin 2016</p>
    </div>
</footer>

<!-- jQuery -->
<script src="<?= WEBROOT; ?>/Web2/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= WEBROOT; ?>/Web2/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="<?= WEBROOT; ?>/Web2/js/jquery.easing.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?= WEBROOT; ?>/Web2/js/home.js"></script>

<script src="<?= WEBROOT; ?>/Web2/js/parallax.js"></script>

<script type="text/javascript">

    $(document).ready(function(){

        //.parallax(xPosition, adjuster, inertia, outerHeight)
        //xPosition - Position horizontale de l'élément (css)
        //adjuster - La position Y de départ
        //inertia - Vitesse en fonction du Scroll. Exemple: 0.1 est 1/10 ème de la vitesse du scroll. 2 = deux fois la vitesse du scroll.
        //outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
        $('.intro').parallax("center", 0, 0.08, false);
        $('.download-section').parallax("center", 0, 0.07, false);
    })
</script>

</body>
</html>
