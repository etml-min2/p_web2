<div class="space-2"></div>

<?php get_flash(); ?>

<div class="container">
    <ul class="nav nav-pills">
        <li class="<?= isset($datas["catFilter"]) ? "" : "active" ?>">
            <a class="cif-menu" href="<?= BASE_URL ?>/cif">Tous</a>
        </li>
        <?php foreach ($datas["cat"] as $metaCat => $cat): ?>
            <li class="<?= isset($datas["catFilter"]) && $datas["catFilter"] == $cat["idCategory"] ? "active" : "" ?> ">
                <a class="cif-menu" href="<?= BASE_URL ?>/cif?cat=<?= $cat["idCategory"] ?>">
                    <?= $cat["catName"] ?>
                    <span class="badge">
                        <?= $cat["cifNb"] ?>
                    </span>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>

<div class="space"></div>

<div class="container">
    <?php
    for ($n = 0; $n < 3; $n++):
        ?>
        <div class="col-md-4">
            <?php
            foreach ($datas["user"] as $metaUsers => $users):
            if ($users["idUser"] % 3 == $n):
            ?>
            <div class="panel panel-primary" style="border: none;">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <a href="<?= BASE_URL ?>/user/<?= $users['idUser'] ?>"><?= $users["usePseudo"]; ?></a>
                    </h3>
                </div>
                <?php
                foreach ($datas["cif"] as $metaCif => $cif):
                    if ($users["usePseudo"] == $cif["usePseudo"]):
                        ?>
                        <a href="<?= BASE_URL . '/cif/' . $cif['idCif'] ?>" class="list-group-item">
                            <div class="cif-title"><?= $cif['cifTitle'] ?></div>
                            <div class="cif-badge">
                                <span class="badge"><?= $cif["catName"]; ?></span>
                            </div>
                        </a>
                    <?php
                    endif;
                endforeach;
                ?>
            </div>
            <?php
            endif;
            endforeach;
            ?>
        </div>
    <?php
    endfor;
    ?>
</div>