<?php get_header(); ?>

<div class="jumbotron element" style="text-align: center;">
    <h1>404 - Page non trouvée</h1>
    <p>&bull; La page que vous recherchez n'existe pas ou plus &bull;</p>
    <a style="color: #000;" href="<?= BASE_URL; ?>/">Retour à l'accueil</a>
</div>

<?php get_footer(); ?>