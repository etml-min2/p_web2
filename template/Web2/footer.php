<!-- jQuery -->
<script src="<?= WEBROOT; ?>/Web2/js/jquery.js" type="text/javascript"></script>

<!-- Custom css -->
<script src="<?= WEBROOT; ?>/Web2/js/view.js" type="text/javascript"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= WEBROOT; ?>/Web2/js/bootstrap.min.js" type="text/javascript"></script>

<!-- Plugin jQuery -->
<script src="<?= WEBROOT; ?>/Web2/js/rating.js" type="text/javascript"></script>

<!-- Modal suitable -->
<script src="<?= WEBROOT; ?>/Web2/js/dialog-polyfill.js" type="text/javascript"></script>
<script src="<?= WEBROOT; ?>/Web2/js/suite.js" type="text/javascript"></script>

</body>
</html>
