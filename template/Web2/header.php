<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/png" href="<?=WEBROOT?>/Web2/img/ic_done_black_16dp_1x.png">

    <title>Projet Web 2</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= WEBROOT; ?>/Web2/css/flatly.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= WEBROOT; ?>/Web2/css/home.css" rel="stylesheet">
    <link href="<?= WEBROOT; ?>/Web2/css/view.css" rel="stylesheet">
    <link href="<?= WEBROOT; ?>/Web2/css/dialog-polyfill.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= WEBROOT; ?>/Web2/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= WEBROOT; ?>/Web2/css/google-fonts-lora.css" rel="stylesheet">
    <link href="<?= WEBROOT; ?>/Web2/css/google-fonts-montserrat.css" rel="stylesheet">
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

<!-- Navigation -->
<nav class="navbar navbar-custom navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="<?= BASE_URL; ?>">
                <i class="fa fa-play-circle"></i> <span class="light">Projet</span> Web 2
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
            <ul class="nav navbar-nav">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li>
                    <a href="<?= BASE_URL; ?>/"><i class="fa fa-home"></i> Accueil</a>
                </li>
                <li>
                    <a href="<?= BASE_URL; ?>/cif"><i class="fa fa-check"></i> CIF</a>
                </li>
                <li>
                    <a href="<?= BASE_URL; ?>/cif/add"><i class="fa fa-plus"></i> Nouveau</a>
                </li>
                <?php if (isset($_SESSION['auth'])) { ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown"
                           id="navLogin"><i class="fa fa-user"></i> <?= $_SESSION['auth']['usePseudo'] ?></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?=BASE_URL?>/user"><i class="fa fa-user"></i> Espace membre</a></li>
                            <li><a href="<?=BASE_URL?>/logout" style="color: darkred;"><i class="fa fa-sign-out"></i> Déconnexion</a></li>
                        </ul>
                    </li>
                <?php } else { ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin"><i class="fa fa-user"></i> Connexion</a>

                        <div class="dropdown-menu" style="padding:17px; width: 300px;">
                            <form class="form" id="formLogin" action="<?=BASE_URL?>/login" method="post">
                                <input class="form-control" name="username" id="username" type="text"
                                       placeholder="Nom d'utilisateur" style="margin-bottom: 15px">
                                <input class="form-control" name="password" id="password" type="password"
                                       placeholder="Mot de passe" style="margin-bottom: 15px">
                                <button type="submit" id="btnLogin" class="btn btn-primary btn-sm">Connexion</button>
                                <button type="button" onclick="location.href='<?=BASE_URL?>/user/add'" id="btnRegister" class="btn btn-default btn-sm pull-right">
                                    Créer un compte
                                </button>
                            </form>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>