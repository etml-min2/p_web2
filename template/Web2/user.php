<!-- 
ETML
Author : beguinju
Date : 20.04.2016
Description : 
-->
<div class="container" style="font-family: montserrat;">
    <div class="page-header">
        <h1 style="margin-top: 100px;">
            <?= $datas['user'][0]['useFirstName'] . " " . $datas['user'][0]['useLastName'] ?>
            <?php if(\App\AppFactory::getSession()->read('auth')['idUser'] == $datas['user'][0]['idUser']): ?>
                <a href="<?= BASE_URL ?>/user/edit" class="btn btn-primary" style="margin-left: 10px;">Modifier</a>
            <?php endif; ?>
        </h1>
    </div>
    <?php get_flash(); ?>
    <div>
        <p class="col-md-6">Nom d'utilisateur : <?= $datas['user'][0]['usePseudo'] ?></p>

        <p class="col-md-6">Date d'inscription
            : <?= date('h:i d.m.Y', strtotime($datas['user'][0]['useRegisterDate'])) ?></p>

        <p class="col-md-6">Nombre de Cif : <?= count($datas['cif']) ?></p>

        <p class="col-md-6">Nombre d'évaluation : <?= count($datas['eval']) ?></p>
    </div>

    <div class="col-md-4">
        <h2 style="text-align: center">Cifs</h2>
        <?php
        if (empty($datas['cif'])):
            echo $datas['user'][0]['usePseudo'] . " n'as fait aucune cif.";
        else:
            ?>
            <div class="panel panel-primary" style="border: none;">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Cifs de <?= $datas['user'][0]["usePseudo"]; ?>
                    </h3>
                </div>
                <?php foreach ($datas['cif'] as $cif): ?>
                    <a href="<?= BASE_URL . '/cif/' . $cif['idCif'] ?>" class="list-group-item">
                        <div class="cif-title"><?= $cif['cifTitle'] ?></div>
                        <div class="cif-badge">
                            <span class="badge"><?= $cif['catName']; ?></span>
                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="col-lg-8">
        <h2 style="text-align: center">Évaluations</h2>
        <?php
        if (empty($datas['cif'])):
            echo $datas['user'][0]['usePseudo'] . " n'as fait aucune évaluation.";
        else:
            foreach ($datas['eval'] as $comment):
                ?>
                <div class="panel panel-primary" style="border: none;">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="<?= BASE_URL . '/cif/' . $comment['fkCif'] ?>">
                                <?= $comment['evaTitle'] ?>
                                <?php for ($i = 0; $i < 5; $i++): ?>
                                    <i class="fa fa-star<?= $i < $comment['evaNote'] ? "" : "-o" ?>"></i>
                                <?php endfor; ?>
                                <span class="light pull-right"><?= $comment['cifTitle'] ?></span>
                            </a>
                        </h3>

                    </div>
                    <?php if (!empty($comment['evaDescription'])): ?>
                        <div class="panel-body">
                            <?= $comment['evaDescription'] ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php
            endforeach;
        endif;
        ?>

    </div>
</div>
