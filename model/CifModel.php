<?php

namespace Model;

use App\Model;

/**
 * Class CifModel
 * Model pour les CIFs
 *
 * @package Model
 */
class CifModel extends Model{

    /**
     * Retourne une CIF par son ID
     * @param $id integer ID de la CIF
     * @return bool|mixed
     */
    public function getCif($id){
        $req = $this->db->prepare('SELECT * FROM t_cif INNER JOIN t_user ON fkUser = idUser INNER JOIN t_category ON fkCategory = t_category.idCategory WHERE idCIF = ?', [$id]);
        if($req->rowCount() > 0)
            return $req->fetch();
        else
            return false;
    }

    /**
     * Supprime une CIF
     * @param $id integer ID de la CIF
     */
    public function deleteCif($id){
        $this->db->prepare('DELETE FROM t_cif WHERE idCIF = ?', [$id]);
    }

    public function addComment($note, $title, $comment, $cif, $user){
        $array = [$note, $title, $comment, $cif, $user];
        $this->db->prepare('INSERT INTO t_evaluation (evaNote, evaTitle, evaDescription, fkCif, fkUser) VALUES (?, ?, ?, ?, ?)', $array);
    }

    public function getComment($id){
        $req = $this->db->prepare('SELECT * FROM t_evaluation INNER JOIN t_user ON fkUser = idUser WHERE fkCif = ? ORDER BY idEvaluations DESC', [$id]);
        if($req->rowCount() > 0)
            return $req->fetchAll(\PDO::FETCH_ASSOC);
        else
            return false;
    }

    public function getLast5Cif(){
        $req = $this->db->prepare('SELECT * FROM t_cif INNER JOIN t_user ON fkUser = idUser INNER JOIN t_category ON fkCategory = t_category.idCategory ORDER BY idCif DESC LIMIT 5');
        if ($req->rowCount() > 0)
            return $req->fetchAll();
        else
            return false;
    }
}