<?php
namespace Model;

use App\Model;

/**
 * Class CategoryModel
 * Model pour les catégories
 *
 * @package Model
 */
class CategoryModel extends Model{

    /**
     * Retourne toutes les catégories
     * @return array Toutes les catégories
     */
    public function getAllCategories(){
        $req = $this->db->prepare('SELECT * FROM t_category');
        return $req->fetchAll();
    }

}