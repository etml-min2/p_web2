<?php

namespace App;

/**
 * Class App
 * Classe mère de l'application qui sert à gérer les actions principales
 *
 * @package App
 */
class App {

    private $route;
    private $data;

    /**
     * Lancement de l'application
     */
    public function run() {
        //Traitement de l'URL
        if (!empty($_GET['url'])) {
            $url = $_GET['url'];
            $this->route = Router::parse($url);
            $controller = $this->route[0];

            //Chargement du contrôleur
            $this->loadController($controller);
        } else {
            $model = AppFactory::getModel('cif');
            $cif = $model->getLast5Cif();
            $this->send('cif', $cif);
            $this->render('index');
        }
    }

    /**
     * Permet de charger un contrôleur
     * @param $controller string contrôleur à charger
     */
    private function loadController($controller) {
        $controller = ucfirst($controller) . 'Controller';
        $file = ROOT . '/controller/' . $controller . '.php';

        //Inclusion du contrôleur et execution de la méthode demandée
        if (file_exists($file)) {
            require $file;
            $controller = '\\Controller\\' . $controller;
            $ctrl = new $controller($this->route);

            //Appel de la fonction
            if (isset($this->route[1]) && method_exists($ctrl, $this->route[1])) {
                call_user_func([$ctrl, $this->route[1]]);
            } else {
                $ctrl->index();
            }
        } else {
            //Page 404 si le contrôleur n'existe pas
            header('HTTP/1.1 404 Not Found');
            $this->render('404');
        }
    }

    /**
     * Permet d'afficher une page spécifique à un thème
     * @param $page string Page à rendre
     */
    protected function render($page) {
        $datas = $this->data;
        require ROOT . '/template/' . Config::getConfig()['theme'] . '/' . $page . '.php';
    }

    /**
     * Permet d'envoyer des données à la vue
     * @param $key string Clé
     * @param $value mixed Valeur
     */
    protected function send($key, $value = null){
        //Si la clé est un tableau, on la stock directement
        if(is_array($key))
            $this->data = $key;
        else
            $this->data[$key] = $value;
    }

}