<?php

namespace App;

/**
 * Class Router
 * Classe qui s'occupe de la gestion des URLs
 *
 * @package app
 **/
class Router {

    /**
     * Permet de parser une URL pour récupérer ses paramètres
     * @param $url string URL à parser
     * @return array Tableau contenant les différents paramètres de l'URL
     */
    public static function parse($url) {
        return explode('/', strtolower(trim($url, '/')));
    }

}