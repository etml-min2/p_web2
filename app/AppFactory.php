<?php

namespace App;

/**
 * Class AppFactory
 * Permet d'accéder aux instances de l'application
 *
 * @package App
 */
class AppFactory {

    /**
     * @var Auth Instance de Auth
     */
    private static $authInstance;

    /**
     * @var Session Instance de Session
     */
    private static $sessionInstance;

    /**
     * @var Database Instance de Database
     */
    private static $databaseInstance;

    /**
     * @var App Instance de App
     */
    private static $appInstance;

    /**
     * Retourne l'instance de la classe Auth
     * @return Auth Instance de la classe Auth
     */
    public static function getAuth() {
        if (is_null(self::$authInstance))
            self::$authInstance = new Auth(self::getSession(), self::getDatabase());
        return self::$authInstance;
    }

    /**
     * @param $model string Model à charger
     * @return mixed
     */
    public static function getModel($model) {
        $model = ucfirst($model) . 'Model';
        $file = ROOT . '/model/' . $model . '.php';
        if (file_exists($file)) {
            require $file;
            $model = '\\Model\\' . $model;
            return new $model(new Database());
        } else {
            return false;
        }
    }

    /**
     * Retourne l'instance de la session
     * @return Session Instance de la session
     */
    public static function getSession() {
        if (is_null(self::$sessionInstance))
            self::$sessionInstance = new Session();
        return self::$sessionInstance;
    }

    /**
     * Retourne l'instance de la connexion à la bdd
     * @return Database Instance de Database
     */
    public static function getDatabase() {
        if (is_null(self::$databaseInstance))
            self::$databaseInstance = new Database();
        return self::$databaseInstance;
    }

    /**
     * Retourne l'instance de l'application
     * @return App Instance de l'application
     */
    public static function getApplication(){
        if (is_null(self::$appInstance))
            self::$appInstance = new App();
        return self::$appInstance;
    }

}