<?php

namespace App;

/**
 * Class Auth
 * Gére l'autentification de l'utilisateur
 *
 * @package App
 */
class Auth {

    /**
     * @var Session Instance de la classe Session
     */
    private $session;

    /**
     * @var Database Instance de la classe Database
     */
    private $db;

    /**
     * Injection de dépendance
     * @param Session $session Instance de Session
     * @param Database $database Instance de Database
     */
    public function __construct(Session $session, Database $database) {
        $this->session = $session;
        $this->db = $database;
    }

    /**
     * @param $username
     * @param $password
     * @param string $redirection
     */
    public function login($username, $password, $redirection = 'cif') {
        //Récupération de l'utilisateur dans la bdd
        $req = $this->db->prepare("SELECT * FROM t_user WHERE usePseudo = ?", [$username]);
        $user = $req->fetch();

        //Si il existe et que le mdp est correct: redirection + message de succès
        if ($req->rowCount() > 0 && password_verify($password, $user['usePassword'])) {
            //Stockage en session des informations
            $this->session->write('auth', $user);
            set_flash('Connexion avec succès.', 'success');
            header('Location:' . BASE_URL . "/$redirection");
            die();
        } else {
            //Message d'erreur et redirection sur login
            set_flash('Nom d\'utilisateur ou mot de passe incorrect', 'danger');
            header('Location:' . BASE_URL . "/login?redirect=$redirection");
            die();
        }
    }

    /**
     * Check si le visiteur est connecté ou non
     * @return bool Est-ce que le visiteur est connecté
     */
    public function isLogged() {
        return $this->session->read('auth') ? true : false;
    }

    /**
     * Restreint l'accès à une page
     */
    public function restrict() {
        if (!$this->session->read('auth')) {
            set_flash('Vous n\'avez pas la permission pour accéder à cette page', 'danger');
            header('Location:' . BASE_URL . '/cif');
            die();
        }
    }
}