<?php

namespace App;

/**
 * Class Session
 * Permet de gérer les informations en session
 *
 * @package App
 */
class Session {

    /**
     * Démarrage de la session si ce n'est pas déjà fait
     */
    public function __construct() {
        if (session_status() != PHP_SESSION_ACTIVE)
            session_start();
    }

    /**
     * Ecrit, dans la session, une valeur pour une certaine clé
     * @param $key string Clé
     * @param $value string Valeur
     */
    public function write($key, $value) {
        $_SESSION[$key] = $value;
    }

    /**
     * Lit une valeur pour une certaine clé
     * @param $key string Clé
     * @return mixed
     */
    public function read($key) {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }

    /**
     * Supprime une valeur en session
     * @param $key string Clé
     */
    public function delete($key) {
        unset($_SESSION[$key]);
    }
}