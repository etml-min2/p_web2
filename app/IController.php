<?php

namespace App;

/**
 * Interface IController
 * @package App
 */
interface IController {

    public function index();

}