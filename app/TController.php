<?php

namespace App;

/**
 * Constructeur commun des contrôleurs
 * Class TController
 * @package App
 */
trait TController {

    /**
     * @var array URL formatée
     */
    private $route;

    /**
     * Constructeur des contrôleurs
     * @param $route array Initialisation de la route
     */
    public function __construct($route){
        $this->route = $route;
    }

}