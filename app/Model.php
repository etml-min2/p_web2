<?php

namespace App;

/**
 * Class Model
 * Parent de tous les modèles
 *
 * @package App
 */
class Model {
    /**
     * @var Database
     */
    protected $db;

    /**
     * Injection de dépendance
     * @param Database $db Instance de Database
     */
    public function __construct(Database $db) {
        $this->db = $db;
    }
}