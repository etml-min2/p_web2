<?php

namespace App;

use PDO;

/**
 * Class Database
 * Permet la gestion de la base de données
 *
 * @package app
 */
class Database {
    /**
     * @var PDO Objet PDO
     */
    private $pdo;

    /**
     * Retourne l'objet PDO
     * @return PDO PDO object
     */
    private function getPDO() {
        if (is_null($this->pdo)) {
            $this->pdo = new PDO('mysql:host=' . Config::getConfig()['db_host'] . ';dbname=' . Config::getConfig()['db_name'], Config::getConfig()['db_user'], Config::getConfig()['db_pass'], [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'']);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        }
        return $this->pdo;
    }

    /**
     * Permet d'executer une requête à la base de donnée
     * @param $req string Requête SQL
     * @return PDOStatement Jeu de résultats
     */
    public function query($req) {
        return $this->getPDO()->query($req);
    }

    /**
     * Execute une requête SQL préparée
     * @param $sql string Requête SQL
     * @param array $attr Attributs de la requête
     * @return \PDOStatement
     */
    public function prepare($sql, $attr = []) {
        //Prépare et execute une requête SQL
        $req = $this->getPDO()->prepare($sql);
        $req->execute($attr);
        return $req;
    }

}