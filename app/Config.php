<?php

namespace App;

/**
 * Class Config
 * Gére le chargement de la configuration
 *
 * @package App
 */
class Config {

    /**
     * @var array Tableau contenant la configuration
     */
    private $settings = [];

    /**
     * @var Config Instance de la classe Config
     */
    private static $_instance;

    /**
     * Retourne l'instance de la configuration
     * @return Config Instance de la configration
     */
    private static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new Config();
        }
        return self::$_instance;
    }

    /**
     * Constructeur qui s'occupe de charger la configuration dans $settings
     */
    private function __construct() {
        $this->settings = require(dirname(__DIR__) . '/config/conf.php');
    }

    /**
     * Charge la configuration dans la variable settings
     * @param string $conf Configuration à charger
     * @return array Tableau de la configuration choisie
     */
    private function get($conf) {
        if (!isset($this->settings[$conf])) {
            return null;
        }
        return $this->settings[$conf];
    }

    /**
     * Permer d'obtenir la configuration sous forme tableau
     * @param string $conf Configuration à charger
     * @return array Tableau contenant la configuration
     */
    public static function getConfig($conf = 'default') {
        return self::getInstance()->get($conf);
    }

}