<?php
namespace Controller;

use App\App;
use App\IController;
use App\Session;

/**
 * Class LogoutController
 * Déconnexion de l'utilisateur
 *
 * @package Controller
 */
class LogoutController extends App implements IController {
    public function index() {
        $session = new Session();
        set_flash('Déconnexion avec succès.', 'success');
        $session->delete('auth');
        header('Location:' . BASE_URL . '/cif');
    }
}