<?php

namespace Controller;

use App\App;
use App\AppFactory;
use App\IController;
use App\TController;

/**
 * Class CifController
 * Gére les actions sur les CIF
 *
 * @package Controller
 */
class CifController extends App implements IController {

    use TController;

    /**
     * Listing des CIF par catégorie ou non
     */
    public function index() {
        //Connexion à la bdd
        $db = AppFactory::getDatabase();

        //Si il n'y a pas d'actions particulières
        if (!isset($this->route[1])) {

            //Requêtes SQL
            $usersRequest = 'SELECT distinct idUser, usePseudo FROM t_user INNER JOIN t_cif ON idUser = fkUser';
            $cifRequest = 'SELECT idCif, usePseudo, cifTitle, catName FROM t_cif INNER JOIN t_user ON fkUser = idUser INNER JOIN t_category ON fkCategory = idCategory';
            $catRequest = 'SELECT idCategory, catName, count(idCIF) as cifNb FROM t_category inner join t_cif on idCategory = fkCategory group by idCategory';
            $sqlAttr = [];

            //Tri par catégorie
            if (isset($_GET["cat"])) {
                $catFilter = addslashes($_GET["cat"]);
                $this->send('catFilter', $catFilter);
                $sqlAttr = [$catFilter];
                $usersRequest .= ' WHERE fkCategory = ?';
                $cifRequest .= ' WHERE fkCategory = ?';
            }

            //Récupération des utilisateurs, des CIFs et des catégories
            $user = $db->prepare($usersRequest, $sqlAttr)->fetchAll();
            $cif = $db->prepare($cifRequest, $sqlAttr)->fetchAll();
            $cat = $db->prepare($catRequest)->fetchAll();

            //Envoi des données à la vue
            $this->send('user', $user);
            $this->send('cif', $cif);
            $this->send('cat', $cat);

            //Chargement de la vue
            get_header();
            $this->render('view');
            get_footer();
        } else if (isset($this->route[1]) && is_numeric($this->route[1])) { //Si il y a une action et que c'est un nombre, affichage d'une CIF particulière

            //Récupération de la CIF avec son ID
            $cifModel = AppFactory::getModel('cif');
            $data = $cifModel->getCif($this->route[1]);
            $comment = $cifModel->getComment($this->route[1]);

            // Calcul de la moyenne des évaluations d'un CIF
            $noteCount = 0;
            $noteAverage = 0;
            // S'il y a au moins 1 commentaire
            if (!empty($comment))
            {
                $noteCount = count($comment);
                // Valeur des somme des évaluations
                $noteTotal = 0;
                for ($k = 0; $k < $noteCount; $k++)
                {
                    // Additions des évaluations
                    $noteTotal += $comment[$k]['evaNote'];
                }
                // Calcul de la moyenne (Somme/Nb CIFs)
                $noteAverage = $noteTotal/$noteCount;
            }
            
            $eval = ['noteCount'=>$noteCount, 'noteAverage'=>$noteAverage];

            //Si elle existe
            if ($data) {

                //Envoi des données à la vue
                $this->send('cif', $data);
                $this->send('comment', $comment);
                $this->send('eval', $eval);

                //Chargement de la vue
                get_header();
                $this->render('detailsCif');
                get_footer();
            } else {
                //Si elle n'existe pas, message d'erreur et redirection
                header('HTTP/1.1 404 Not Found');
                $this->render('404');
            }
        } else {
            //L'action est inconnue, 404 not found
            header('HTTP/1.1 404 Not Found');
            $this->render('404');
        }
    }

    /**
     * Ajout d'une CIF
     */
    public function add() {
        //Vérification si l'utilisateur est connecté
        if (AppFactory::getAuth()->isLogged()) {

            //Connexion à la bdd
            $db = AppFactory::getDatabase();

            //Si un formulaire a été posté, on le traite sinon affiche le formulaire
            if (!empty($_POST)) {

                //Récupération et traitement des valeur du formulaire
                $title = htmlentities($_POST['title'], ENT_QUOTES);
                $desc = $_POST['description'];
                $cat = $_POST['category'];

                //Récupération de l'ID de l'utilisateur connecté en session
                $user = AppFactory::getSession()->read('auth')['idUser'];

                //Ajout de la CIF dans la bdd
                $db->prepare('INSERT INTO t_cif (cifTitle, cifDescription, fkUser, fkCategory) VALUES (?, ?, ?, ?)', [$title, $desc, $user, $cat]);

                //Message de succès et redirection
                set_flash('Cif ajouté avec succès.', 'success');
                header('Location:' . BASE_URL . '/cif');
                die();
            } else {

                //Récupération de toutes les catégories
                $data = AppFactory::getModel('category')->getAllCategories();

                //Affichage du formulaire
                $this->send($data);
                get_header();
                $this->render('addCif');
                get_footer();
            }
        } else {
            //Message d'avertissement et redirection
            set_flash('Merci de bien vouloir vous connecter pour ajouter une CIF', 'warning');
            header('Location:' . BASE_URL . '/login?redirect=cif/add');
            die();
        }
    }

    /**
     * Modification d'une CIF
     */
    public function edit() {
        //Vérification si l'ID de la CIF est bien spécifié
        if (isset($this->route[2])) {

            $cifModel = AppFactory::getModel('cif');
            $cif = $cifModel->getCif($this->route[2]);

            //Vérification si l'utilisateur est connecté
            if (AppFactory::getAuth()->isLogged()) {

                //Vérification si l'utilisateur est le propriétaire de la CIF
                if (AppFactory::getSession()->read('auth')['idUser'] == $cif['fkUser']) {

                    //Connexion à la bdd
                    $db = AppFactory::getDatabase();

                    //Si le formulaire est posté, on le traite, sinon on affiche le formulaire
                    if (!empty($_POST)) {

                        //Récupération et traitement des valeur du formulaire
                        $title = htmlentities($_POST['title'], ENT_QUOTES);
                        $category = $_POST['category'];
                        $description = $_POST['description'];

                        //Modification de la CIF dans la bdd
                        $db->prepare('UPDATE t_cif SET cifTitle = ?, cifDescription = ?, fkCategory = ? WHERE idCIF = ?', [$title, $description, $category, $this->route[2]]);

                        //Message de succès et redirection
                        set_flash('CIF modifiée avec succès');
                        header('Location:' . BASE_URL . '/cif/' . $this->route[2]);
                        die();
                    } else {

                        //Modèles des catégories
                        $categoryModel = AppFactory::getModel('category');

                        //Récupération des catégories
                        $categories = $categoryModel->getAllCategories();

                        //Test si l'utilisateur connecté est propriétaire de cette CIF
                        if (AppFactory::getSession()->read('auth') != $cif['fkUser'])

                            //Si la CIF existe, affichage du formulaire
                            if ($cif) {
                                //Envoi des données à la vue
                                $this->send('cif', $cif);
                                $this->send('categories', $categories);

                                get_header();
                                $this->render('editCif');
                                get_footer();
                            } else {
                                //La CIF est inconnue, 404 not found
                                header('HTTP/1.1 404 Not Found');
                                $this->render('404');
                            }
                    }
                } else {
                    //Message d'erreur et redirection
                    set_flash('Vous n\'avez la permission d\'éditer cette CIF.', 'danger');
                    header('Location:' . BASE_URL . '/cif/' . $this->route[2]);
                    die();
                }
            } else {
                //Message d'avertissement et redirection
                set_flash('Merci de bien vouloir vous connecter pour modifier cette CIF', 'warning');
                header('Location:' . BASE_URL . '/login?redirect=cif/edit/' . $this->route[2]);
                die();
            }
        } else {
            //L'action est inconnue, 404 not found
            header('HTTP/1.1 404 Not Found');
            $this->render('404');
        }
    }

    /**
     * Suppression d'une CIF
     */
    public function delete() {
        //Verification si l'ID existe
        if (isset($this->route[2])) {

            //Récupération de la CIF
            $cifModel = AppFactory::getModel('cif');
            $cif = $cifModel->getCif($this->route[2]);

            //Verification si l'utilisateur est connecté
            if (AppFactory::getAuth()->isLogged()) {

                //Vérification si l'utilisateur est le propriétaire de la CIF
                if (AppFactory::getSession()->read('auth')['idUser'] == $cif['fkUser']) {

                    //Suppression de la CIF
                    $cifModel->deleteCif($this->route[2]);

                    //Message de succès et redirection
                    set_flash('CIF supprimée avec succès');
                    header('Location:' . BASE_URL . '/cif');
                    die();
                } else {
                    //Message d'erreur et redirection
                    set_flash('Vous n\'avez la permission pour supprimer cette CIF.', 'danger');
                    header('Location:' . BASE_URL . '/cif/' . $this->route[2]);
                    die();
                }
            } else {
                //Message d'erreur et redirection
                set_flash('Vous n\'avez la permission pour supprimer cette CIF.', 'danger');
                header('Location:' . BASE_URL . '/login?redirect=cif/' . $this->route[2]);
                die();
            }
        } else {
            //Pas d'ID, 404 not found
            header('HTTP/1.1 404 Not Found');
            $this->render('404');
        }
    }

    //Todo: commentaires (Béguin)
    public function addComment() {
        if (!empty($_SESSION['auth'])) {
            if (!empty($_POST) && isset($_GET['id'])) {
                $note = isset($_POST['note']) ? $_POST['note'] : 0;
                $comment = htmlentities($_POST['comment'], ENT_QUOTES);
                $title = $_POST['title'];
                $cif = $_GET['id'];
                $user = $_SESSION['auth']['idUser'];

                $cifModel = AppFactory::getModel('cif');
                $cifModel->addComment($note, $title, $comment, $cif, $user);
                set_flash('Commentaire ajouté avec succès.', 'success');
            } else {
                set_flash('Impossible d\'ajouter le commentaire.', 'danger');
            }
            header('Location:' . BASE_URL . '/cif/' . $cif);
        } else {
            set_flash('Vous n\'avez pas la permission pour accéder à cette page', 'danger');
            header('Location:' . BASE_URL . '/login?redirect=cif');
            die();
        }

    }

}