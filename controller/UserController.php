<?php
namespace Controller;

use App\App;
use App\AppFactory;
use App\IController;
use App\TController;

class UserController extends App implements IController {

    use TController;

    public function index() {
        $auth = AppFactory::getAuth();
        if ($auth->isLogged()) {
            $db = AppFactory::getDatabase();
            if (isset($this->route[1])) {
                $id = $this->route[1];
            } else {
                $id = $_SESSION['auth']['idUser'];
            }

            $userRequest = $db->query("SELECT * FROM t_user WHERE idUser = " . $id);
            $cifRequest = $db->prepare("SELECT * FROM t_cif INNER JOIN t_category ON fkCategory = idCategory WHERE fkUser = ?", [$id]);
            $evalRequest = $db->prepare("SELECT * FROM t_evaluation INNER JOIN t_cif ON fkCif = idCif WHERE t_evaluation.fkUser = ?", [$id]);
            $user = $userRequest->fetchAll(\PDO::FETCH_ASSOC);
            $cif = $cifRequest->fetchAll(\PDO::FETCH_ASSOC);
            $eval = $evalRequest->fetchAll(\PDO::FETCH_ASSOC);

            $this->send('user', $user);
            $this->send('cif', $cif);
            $this->send('eval', $eval);

            if (empty($user)) {
                $this->render('404');
            } else {
                get_header();
                $this->render('user');
                get_footer();
            }
        } else {
            header('Location:' . BASE_URL . '/login?redirect=user');
        }
    }

    public function edit() {
        $auth = AppFactory::getAuth();
        $db = AppFactory::getDatabase();
        if ($auth->isLogged()) {
            if (!empty($_POST)) {
                $allUsersRequest = $db->query("SELECT usePseudo FROM t_user");
                $allUsers = $allUsersRequest->fetchAll(\PDO::FETCH_ASSOC);

                $username = $_POST['add_username'];
                $password = $_POST['add_password'];
                $passwordConfirm = $_POST['add_passwordConfirm'];
                $firstName = $_POST['add_firstName'];
                $lastName = $_POST['add_lastName'];

                $isUnique = 0;
                if ($_SESSION['auth']['usePseudo'] != $username) {
                    for ($i = 0; $i < count($allUsers); $i++) {
                        if ($allUsers[$i]['usePseudo'] == $username) {
                            $isUnique++;
                        }
                    }
                }

                if ($isUnique == 0) {
                    if ($password == $passwordConfirm) {
                        $password = password_hash($password, PASSWORD_DEFAULT);
                        $addUser = $db->prepare("UPDATE t_user SET usePseudo=?, usePassword=?, useFirstName=?, useLastName=? WHERE idUser =" . $_SESSION['auth']['idUser'], [$username, $password, $firstName, $lastName]);
                        set_flash('Vos informations ont été modifié avec succès.', 'success');
                        header('Location:' . BASE_URL . '/user');
                        die();
                    } else {
                        set_flash('Les mots de passe ne correspondent pas.', 'danger');
                        header('Location:' . BASE_URL . '/user/edit');
                        die();
                    }
                } else {
                    set_flash('Ce nom d\'utilisateur n\'est pas disponible.', 'danger');
                    header('Location:' . BASE_URL . '/user/edit');
                    die();
                }
            } else {
                $userRequest = $db->query("SELECT * FROM t_user WHERE idUser =" . $_SESSION['auth']['idUser']);
                $user = $userRequest->fetchAll(\PDO::FETCH_ASSOC)[0];
                $this->send($user);
                get_header();
                $this->render('addUser');
                get_footer();
            }
        } else {
            header('Location:' . BASE_URL . '/login?redirect=user/edit');
        }
    }

    public function add() {
        if (!empty($_POST)) {
            $db = AppFactory::getDatabase();

            $allUsersRequest = $db->query("SELECT usePseudo FROM t_user");
            $allUsers = $allUsersRequest->fetchAll(\PDO::FETCH_ASSOC);

            $username = $_POST['add_username'];
            $password = $_POST['add_password'];
            $passwordConfirm = $_POST['add_passwordConfirm'];
            $firstName = $_POST['add_firstName'];
            $lastName = $_POST['add_lastName'];

            $isUnique = 0;
            for ($i = 0; $i < count($allUsers); $i++) {
                if ($allUsers[$i]['usePseudo'] == $username) {
                    $isUnique++;
                }
            }

            if ($isUnique == 0) {
                if ($password == $passwordConfirm) {
                    $password = password_hash($password, PASSWORD_DEFAULT);
                    $db->prepare('INSERT INTO t_user (usePseudo, usePassword, useFirstName, useLastName) VALUES (?, ?, ?, ?)', [$username, $password, $firstName, $lastName]);
                    set_flash('Utilisateur créé avec succès.', 'success');
                    header('Location:' . BASE_URL . '/cif');
                    die();
                } else {
                    set_flash('Les mots de passe ne correspondent pas.', 'danger');
                    header('Location:' . BASE_URL . '/user/add');
                    die();
                }
            } else {
                set_flash('Ce nom d\'utilisateur n\'est pas disponible.', 'danger');
                header('Location:' . BASE_URL . '/user/add');
                die();
            }
        } else {
            get_header();
            $this->render('addUser');
            get_footer();
        }
    }
}