<?php
namespace Controller;

use App\App;
use App\AppFactory;
use App\IController;
use App\TController;

/**
 * Class LoginController
 * Connexion de l'utilisateur
 *
 * @package Controller
 */
class LoginController extends App implements IController {

    /**
     * Connexion de l'utilisateur
     */
    public function index() {
        //Si le formulaire est posté, on tente de connecter l'utilisateur, sinon on affiche le formulaire
        if (!empty($_POST)) {

            //Connexion de l'utilisateur
            $auth = AppFactory::getAuth();
            if (isset($_GET['redirect']))
                $auth->login($_POST['username'], $_POST['password'], $_GET['redirect']);
            else
                $auth->login($_POST['username'], $_POST['password']);
        } else {
            //Affichage du formulaire de connexion
            get_header();
            $this->render('login');
            get_footer();
        }
    }

}